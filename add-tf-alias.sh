# Add tf alias
echo "alias tf='terraform'" >> ~/.bashrc && source ~/.bashrc
echo "alias tfp='terraform plan'" >> ~/.bashrc && source ~/.bashrc
echo "alias tff='terraform fmt'" >> ~/.bashrc && source ~/.bashrc
echo "alias tfv='terraform validate'" >> ~/.bashrc && source ~/.bashrc
echo "alias tfa='terraform apply'" >> ~/.bashrc && source ~/.bashrc