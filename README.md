# terraform-aws-starter

- [Run a new Gitpod workspace](#run-a-new-gitpod-workspace)
- [Install Terraform](#install-terraform)
- [Add Terraform alias (Optional)](#add-Terraform-alias-(optional))

## Run a new Gitpod workspace

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/bayupw/terraform-aws-starter/)

## Install Terraform

```bash
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update
sudo apt-get install terraform
```

or run script
```bash 
chmod +x install.terraform.sh
./install.terraform.sh
```

## Add Terraform alias (Optional)

```bash
echo "alias tf='terraform'" >> ~/.bashrc && source ~/.bashrc
echo "alias tfp='terraform plan'" >> ~/.bashrc && source ~/.bashrc
echo "alias tff='terraform fmt'" >> ~/.bashrc && source ~/.bashrc
echo "alias tfv='terraform validate'" >> ~/.bashrc && source ~/.bashrc
echo "alias tfa='terraform apply'" >> ~/.bashrc && source ~/.bashrc
```

Terraform registry link: https://registry.terraform.io/